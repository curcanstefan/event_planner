<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attendance
 *
 * @ORM\Table(name="attendance")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttendanceRepository")
 */
class Attendance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="attended", type="boolean")
     */
    private $attended;

    /**
     * @var bool
     *
     * @ORM\Column(name="filledForm", type="boolean")
     */
    private $filledForm;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PaymentType")
     */
    private $payMethod;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Person")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event")
     */
    protected $event;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attended
     *
     * @param boolean $attended
     * @return Attendance
     */
    public function setAttended($attended)
    {
        $this->attended = $attended;

        return $this;
    }

    /**
     * Get attended
     *
     * @return boolean 
     */
    public function getAttended()
    {
        return $this->attended;
    }

    /**
     * Set filledForm
     *
     * @param boolean $filledForm
     * @return Attendance
     */
    public function setFilledForm($filledForm)
    {
        $this->filledForm = $filledForm;

        return $this;
    }

    /**
     * Get filledForm
     *
     * @return boolean 
     */
    public function getFilledForm()
    {
        return $this->filledForm;
    }

    /**
     * Set payMethod
     *
     * @param integer $payMethod
     * @return Attendance
     */
    public function setPayMethod($payMethod)
    {
        $this->payMethod = $payMethod;

        return $this;
    }

    /**
     * Get payMethod
     *
     * @return integer 
     */
    public function getPayMethod()
    {
        return $this->payMethod;
    }

    /**
     * @param mixed $person
     * @return Attendance
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $event
     * @return Attendance
     */
    public function setEvent($event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }
}
